---
title: "Förhållandena runt Imamens(AJ) födelse"
path: "/fodelsen"
date: "2009-09-05"
category: "Imam Mahdi(AJ)"
tags:
    - "Imamen"

link: http://islamportalen.se/2009/09/05/f%c3%b6delsen/
author: ImamMahdi.se
description: 
post_id: 4
created: 2009/09/05 22:11:48
created_gmt: 2009/09/05 21:11:48
comment_status: closed
post_name: f%c3%b6delsen
status: publish
post_type: post
---

Existensen av den tolfte Imamen(AJ) är likt en sol, vars bländade strålar lyser och skiner i varje shiamuslims hjärta. Abbasiterna hade fått reda på att Imamerna(A) var tolv till antalet, och att den tolfte och sista skulle vara son till [den elfte] Imam Askari(A) vars mystik skulle komma att sträcka sig över tiderna, och att det är han(AJ) som kommer att forma världsherraväldet. De tyranniska makthavarna var starkt oroade och skrämda över födseln av detta barn och var beredda att göra allt för att stoppa det. Men de tänkte inte på att Farao med all sin makt och styrka, och med alla barbariska massakrer på barn och alla nyfödda bebisar inte kunde stoppa Guds vilja. Han letade och sökte efter Moses(A) från hus till hus ovetande om att den han sökte efter fanns närmare än vad han trodde, nämligen i hans palats och sittandes på hans egna knän. Den abbasitiska kalifen, Mo'tamid, som kan liknas vid den tidens farao, var extremt vaksam och bevakade Imam Askaris(A) hus. När den elfte Imamen(A) blev förgiftad och fördes från fängelset till sitt hus, i ett sjukt tillstånd, skickade kalifen med fem hovmän som skulle observera och informera honom om minsta lilla händelse eller incident som sker vid Imamens(A) hus. Han sände till och med en grupp barnmorskor till den elfte Imamens(A) hus så att de kunde hålla ett öga på Imamens(A) fru. Imam Askaris(A) fru sjönk i djup sorg och oroligheter efter martyrskapet av vår elfte Imam(A). Alla stängde sina verksamheter i sorg och skyndade mot Imamens(A) hus. De bar upp Imamens(A) döda kropp under begravningsceremonin som blev mycket praktfull och storslagen. Den abbasidiske kalifen blev väldigt oroad och förbryllad över de stora massorna som deltog, och gjorde sitt bästa för att dölja sin inblandning i Imamens(A) död samtidigt som han försökte få martyrskapet att se ut som en naturlig dödsorsak. Mo'tamid sände sin bror för att delta i begravnings ritualerna för att på så sett visa att han inte hade något med Imamens(A) död att göra. Samtidigt, tog han på sig ansvaret att dela ut Imamens(A) rikedomar för att på så sätt visa att Imamen(A) inte hade lämnat efter sig någon son som kunde vara arvinge till hans tillgångar eller be begravningsbönen. Men trots alla ansträngningar var det Guds vilja som skulle ske och fastän Imamens(A) son endast var fem år gammal när hans fader blev martyr. Så blev han(AJ) Imam, precis som Jesus Kristus(A) som utnämndes till profet redan när han var i vaggan. När de begravde kroppen var det Imam Askaris(A) son som vid sin unga ålder förde ner hans fars kropp till jorden. När hans farbror, som inte var en god man, ställde sig för att leda bönen över hans far, flyttade han sin farbror åt sidan och bad själv begravningsbönen över sin far. Efter bönen försvann Imam Mahdi(AJ) från människors åsyn. Och det var redan under hans(AJ) fars, Imam Askaris(A) tid som folk hade sett honom i hans(AJ) fars hus och hört hans fars rekommendationer om sin son. Efter faderns martyrskap behöll de kontakten med honom under en lång tid framåt.