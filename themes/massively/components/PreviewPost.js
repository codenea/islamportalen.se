import React from 'react';
import Link from 'gatsby-link';
import Tags from './Tags';

export default class PreviewPost extends React.Component {

  constructor(props) {
    super(props);
  }

  getThumbnail() {
    if (this.props.post.frontmatter.thumbnailImage != null) {
        var thumbnail = "./" + this.props.post.frontmatter.thumbnailImage.publicURL;
        return (<Link to={this.props.post.frontmatter.path} className="image fit"><img src={thumbnail} alt="" /></Link>)
    }
  }

  render() {
    return (
      <article>
        <header>
          <span className="date">{this.props.post.frontmatter.date}</span>
          <h2>
            <Link to={this.props.post.frontmatter.path}>
              {this.props.post.frontmatter.title}
            </Link>
          </h2>
          <Tags list={this.props.post.frontmatter.tags || []} />
        </header>

        { this.getThumbnail() }

        <p>
          {this.props.post.excerpt}
        </p>
        <ul className="actions">
          <li>
            <Link to={this.props.post.frontmatter.path} className="button">Full Story</Link>
          </li>
        </ul>
      </article>
    );
  }
}